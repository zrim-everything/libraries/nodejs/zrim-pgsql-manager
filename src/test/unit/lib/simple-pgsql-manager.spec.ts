import {SimplePostGreSqlManager} from '../../../lib/simple-pgsql-manager';
import {SimpleConnectableObject} from 'zrim-base-objects';
import {common as commonErrors} from 'zrim-errors';
import mockRequire = require("mock-require");
import * as pgPromise from 'pg-promise';
import {postGreSqlManager} from "../../../lib";


describe('#SimplePostGreSqlManager', function () {
  function createInstance(): SimplePostGreSqlManager {
    return new SimplePostGreSqlManager();
  }

  describe('#flags', function () {
    describe('#get', function () {
      it("Must return expected value", function () {
        const instance = createInstance();
        const flags = {
          readOnly: true
        };
        spyOn(instance, 'getFlags').and.returnValue(flags);
        const flagsResponse = instance.flags;
        expect(flagsResponse).toBe(flags);
        expect(instance.getFlags).toHaveBeenCalledTimes(1);
      });
    }); // #get
  }); // #flags

  describe('#database', function () {
    describe('#get', function () {
      it("Must return expected value", function () {
        const instance = createInstance();
        instance['properties'].database = {
          // @ts-ignore
          k: 41
        };
        expect(instance.database).toBe(instance['properties'].database);
      });
    }); // #get
  }); // #database

  describe('#getFlags', function () {
    it("Must return expected value", function () {
      const instance = createInstance();
      instance['properties'].flags = {
        readOnly: true
      };
      expect(instance.getFlags()).toEqual(instance['properties'].flags);
    });
  }); // #getFlags

  describe('#isReadOnly', function () {
    it('Given flag not defined Then must return false', function () {
      const instance = createInstance();

      spyOn(instance, 'getFlags').and.returnValue({});
      expect(instance.isReadOnly()).toBeFalsy();
      expect(instance.getFlags).toHaveBeenCalled();
    });

    it('Given flag readOnly to be false Then must return false', function () {
      const instance = createInstance();

      spyOn(instance, 'getFlags').and.returnValue({
        readOnly: false
      });
      expect(instance.isReadOnly()).toBeFalsy();
      expect(instance.getFlags).toHaveBeenCalled();
    });

    it('Given flag readOnly to be true Then must return true', function () {
      const instance = createInstance();

      spyOn(instance, 'getFlags').and.returnValue({
        readOnly: true
      });
      expect(instance.isReadOnly()).toBeTruthy();
      expect(instance.getFlags).toHaveBeenCalled();
    });
  }); // #isReadOnly

  describe('#initialize', function () {
    it("Then must call super", async function () {
      const instance = createInstance();

      spyOn(SimpleConnectableObject.prototype, 'initialize').and.callFake(() => Promise.resolve());

      const options = {
        connectionUrl: 'alal'
      };
      await instance.initialize(options);
      expect(SimpleConnectableObject.prototype.initialize).toHaveBeenCalledTimes(1);
      expect(SimpleConnectableObject.prototype.initialize).toHaveBeenCalledWith(options);
    });
  }); // #initialize

  describe('#_handleInitialization', function () {
    it("Given invalid url Then must return error", async function () {
      const instance = createInstance();

      // @ts-ignore
      spyOn(SimpleConnectableObject.prototype, '_handleInitialization').and.callFake(() => Promise.resolve());

      const options = {
        connectionUrl: 'alal'
      };
      try {
        await instance['_handleInitialization'](options, 12);
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.IllegalArgumentError));
        expect(SimpleConnectableObject.prototype['_handleInitialization']).toHaveBeenCalledWith(options, 12);
      }
    });

    it('Given valid options (all) Then must return success', async function () {
      const instance = createInstance();

      const expectedDriver = {
        a: 98
      };
      const pgPromiseMock = jasmine.createSpy('pg-promise').and.returnValue(expectedDriver);
      mockRequire('pg-promise', pgPromiseMock);

      // @ts-ignore
      spyOn(SimpleConnectableObject.prototype, '_handleInitialization').and.callFake(() => new Promise(resolve => setImmediate(resolve)));

      const options = {
        connectionUrl: 'postgresql://localhost/unitTest',
        nativeDriverOptions: {
          a: 5555
        },
        flags: {
          readOnly: true
        }
      };

      await instance['_handleInitialization'](options);
      expect(SimpleConnectableObject.prototype['_handleInitialization']).toHaveBeenCalledTimes(1);
      expect(SimpleConnectableObject.prototype['_handleInitialization']).toHaveBeenCalledWith(options);
      expect(pgPromiseMock).toHaveBeenCalledTimes(1);
      expect(pgPromiseMock).toHaveBeenCalledWith(options.nativeDriverOptions);
      expect(instance['properties'].pgsqlDriver).toBe(expectedDriver);
      expect(instance['properties'].pgsqlOptions).toEqual({
        connectionUrl: options.connectionUrl
      });
      expect(instance['properties'].flags).toEqual({
        readOnly: true
      });
      expect(instance['properties'].flags).not.toBe(options.flags);
      mockRequire.stopAll();
    });

    it('Given valid options (min) Then must return success', async function () {
      const instance = createInstance();

      const expectedDriver = {
        a: 98
      };
      const pgPromiseMock = jasmine.createSpy('pg-promise').and.returnValue(expectedDriver);
      mockRequire('pg-promise', pgPromiseMock);

      // @ts-ignore
      spyOn(SimpleConnectableObject.prototype, '_handleInitialization').and.callFake(() => new Promise(resolve => setImmediate(resolve)));

      const options = {
        connectionUrl: 'postgresql://localhost/unitTest'
      };

      await instance['_handleInitialization'](options);
      expect(SimpleConnectableObject.prototype['_handleInitialization']).toHaveBeenCalledTimes(1);
      expect(SimpleConnectableObject.prototype['_handleInitialization']).toHaveBeenCalledWith(options);
      expect(pgPromiseMock).toHaveBeenCalledTimes(1);
      expect(pgPromiseMock).toHaveBeenCalledWith({});
      expect(instance['properties'].pgsqlDriver).toBe(expectedDriver);
      expect(instance['properties'].pgsqlOptions).toEqual({
        connectionUrl: options.connectionUrl
      });
      expect(instance['properties'].flags).toEqual({});
      mockRequire.stopAll();
    });
  }); // #_handleInitialization

  describe('#_handleDisconnection', function () {
    it('Then must call pool end and remove property', async function () {
      const instance = createInstance();

      const databaseMock = {
        $pool: {
          end: jasmine.createSpy('end')
        }
      };

      // @ts-ignore
      instance['properties'].database = databaseMock;
      await instance['_handleDisconnection']();
      expect(databaseMock.$pool.end).toHaveBeenCalledTimes(1);
      expect(instance['properties'].database).toBeUndefined();
    });
  }); // #_handleDisconnection

  describe('#_handleDataBaseInitialization', function () {
    it('Given valid options Then must return success', async function () {
      const instance = createInstance();

      const options = {
        client: {}
      };
      // @ts-ignore
      await instance['_handleDataBaseInitialization'](options);
      expect(true).toBeTruthy();
    });
  }); // #_handleDataBaseInitialization

  describe('#_isDataBaseInitialize', function () {
    it('Given valid options Then must return success', async function () {
      const instance = createInstance();

      const options = {
        client: {}
      };
      // @ts-ignore
      const response = await instance['_isDataBaseInitialize'](options);
      expect(response).toEqual({
        initialized: true
      });
    });
  }); // #_isDataBaseInitialize

  describe('#_handleCheckDataBaseHealth', function () {
    it('Given valid options and query fails Then must return healthy=false', async function () {
      const instance = createInstance();

      const expectedError = new Error('Unit Test - Fake error');

      // @ts-ignore
      const client = {
        query: jasmine.createSpy('query').and.callFake(() => Promise.reject(expectedError))
      } as pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;

      const options = {
        checkOperations: [{name: "strong", options: undefined}],
        client
      };

      const response = await instance['_handleCheckDataBaseHealth'](options);
      expect(options.client.query).toHaveBeenCalledTimes(1);
      expect(options.client.query).toHaveBeenCalledWith('SELECT 1');
      expect(response).toEqual({
        healthy: false,
        error: expectedError
      });
    });

    it('Given valid options and query success Then must return success', async function () {
      const instance = createInstance();

      // @ts-ignore
      const client = {
        query: jasmine.createSpy('query').and.callFake(() => Promise.resolve())
      } as pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;

      const options = {
        checkOperations: [{name: "strong", options: undefined}],
        client
      };
      const response = await instance['_handleCheckDataBaseHealth'](options);
      expect(options.client.query).toHaveBeenCalledTimes(1);
      expect(options.client.query).toHaveBeenCalledWith('SELECT 1');
      expect(response).toEqual({
        healthy: true
      });
    });

    it('Given client null  Then must return the error of the connection', async function () {
      const instance = createInstance();
      const errorTest = new Error("Test");

      const options = {
        checkOperations: [{name: "strong", options: undefined}],
        connectionError: errorTest
      };
      const response = await instance['_handleCheckDataBaseHealth'](options);
      expect(response).toEqual({
        healthy: false,
        error: errorTest
      });
    });
  }); // #_handleCheckDataBaseHealth

  describe('#_checkDataBaseConnection', function () {
    it('Given valid options and query fails Then must return healthy=false', async function () {
      const instance = createInstance();

      const expectedError = new Error('Unit Test - Fake error');
      // @ts-ignore
      const client = {
        query: jasmine.createSpy('query').and.callFake(() => Promise.reject(expectedError))
      } as pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;

      const options = {
        client
      };
      const response = await instance['_checkDataBaseConnection'](options);
      expect(options.client.query).toHaveBeenCalledTimes(1);
      expect(options.client.query).toHaveBeenCalledWith('SELECT 1');
      expect(response).toEqual({
        valid: false
      });
    });

    it('Given valid options and query success Then must return success', async function () {
      const instance = createInstance();

      // @ts-ignore
      const client = {
        query: jasmine.createSpy('query').and.callFake(() => Promise.resolve())
      } as pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;

      const options = {
        client
      };
      const response = await instance['_checkDataBaseConnection'](options);
      expect(options.client.query).toHaveBeenCalledTimes(1);
      expect(options.client.query).toHaveBeenCalledWith('SELECT 1');
      expect(response).toEqual({
        valid: true
      });
    });
  }); // #_checkDataBaseConnection

  describe('#checkDataBaseHealth', function () {
    it('Given state not ready Then must return error', async function () {
      const instance = createInstance();

      spyOn(instance, 'isReady').and.returnValue(false);

      const options = {
        checkOperations: [{name: "strong", options: undefined}]
      };
      try {
        await instance.checkDataBaseHealth(options);
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.NotReadyError));
        expect(instance.isReady).toHaveBeenCalled();
      }
    });

    it('Given connect to fail Then _handleCheckDataBaseHealth must have connectionError', async function () {
      const instance = createInstance();

      spyOn(instance, 'isReady').and.returnValue(true);

      const errorTest = new Error("Unit Test - Fake error");
      // @ts-ignore
      const database = {
        connect: jasmine.createSpy('connect').and.callFake(() => Promise.reject(errorTest))
      } as pgPromise.IDatabase<postGreSqlManager.DriverExtension>;

      instance['properties'].database = database;

      const options = {
        checkOperations: [{name: "strong", options: undefined}],
        inputContext: {
          c: 12
        }
      };
      // @ts-ignore
      spyOn(instance, '_handleCheckDataBaseHealth').and.callFake(() => Promise.resolve({succeed: true, error: undefined, healthy: false}));

      const response = await instance.checkDataBaseHealth(options);
      expect(database.connect).toHaveBeenCalledTimes(1);
      expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledWith({
        checkOperations: [{name: "strong", options: undefined}],
        connectionError: errorTest,
        inputContext: options.inputContext,
        healthCheckOptions: undefined
      });
      expect(response.healthy).toBe(false);
    });

    it('Given _handleCheckDataBaseHealth to fail Then must return error', async function () {
      const instance = createInstance();
      spyOn(instance, 'isReady').and.returnValue(true);

      const expectedError = new Error('Unit Test - Fake error'),
        clientMock = {
          done: jasmine.createSpy('done')
        };

      // @ts-ignore
      const database = {
        connect: jasmine.createSpy('connect').and.callFake(() => Promise.resolve(clientMock))
      } as pgPromise.IDatabase<postGreSqlManager.DriverExtension>;

      instance['properties'].database = database;

      // @ts-ignore
      spyOn(instance, '_handleCheckDataBaseHealth').and.callFake(() => Promise.reject(expectedError));
      const options = {
        checkOperations: [{name: "strong", options: undefined}]
      };
      try {
        await instance.checkDataBaseHealth(options);
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(database.connect).toHaveBeenCalledTimes(1);
        expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledTimes(1);
        expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledWith({
          checkOperations: [{name: "strong", options: undefined}],
          client: clientMock,
          inputContext: undefined,
          healthCheckOptions: undefined
        });
        expect(clientMock.done).toHaveBeenCalledTimes(1);
      }
    });

    it('Given _handleCheckDataBaseHealth to return healthy=false Then must return expected value', async function () {
      const instance = createInstance();
      spyOn(instance, 'isReady').and.returnValue(true);

      const clientMock = {
        done: jasmine.createSpy('done')
      };

      // @ts-ignore
      const database = {
        connect: jasmine.createSpy('connect').and.callFake(() => Promise.resolve(clientMock))
      } as pgPromise.IDatabase<postGreSqlManager.DriverExtension>;

      instance['properties'].database = database;

      // @ts-ignore
      spyOn(instance, '_handleCheckDataBaseHealth').and.callFake(() => Promise.resolve({
        healthy: false
      }));

      const options = {
        checkOperations: [{name: "strong", options: undefined}]
      };
      const response = await instance.checkDataBaseHealth(options);
      expect(response).toEqual({
        healthy: false,
        checkHealthResult: undefined
      });
      expect(database.connect).toHaveBeenCalledTimes(1);
      expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledTimes(1);
      expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledWith({
        checkOperations: [{name: "strong", options: undefined}],
        client: clientMock,
        inputContext: undefined,
        healthCheckOptions: undefined
      });
      expect(clientMock.done).toHaveBeenCalledTimes(1);
    });

    it('Given _handleCheckDataBaseHealth to return healthy=true Then must return expected value', async function () {
      const instance = createInstance();
      spyOn(instance, 'isReady').and.returnValue(true);
      const clientMock = {
        done: jasmine.createSpy('done')
      };

      // @ts-ignore
      const database = {
        connect: jasmine.createSpy('connect').and.callFake(() => Promise.resolve(clientMock))
      } as pgPromise.IDatabase<postGreSqlManager.DriverExtension>;

      instance['properties'].database = database;

      // @ts-ignore
      spyOn(instance, '_handleCheckDataBaseHealth').and.callFake(() => Promise.resolve({
        healthy: true,
        checkHealthResult: {
          po: 45
        }
      }));

      const options = {
        checkOperations: [{name: "strong", options: undefined}]
      };
      const response = await instance.checkDataBaseHealth(options);
      expect(response).toEqual({
        healthy: true,
        checkHealthResult: {
          po: 45
        }
      });
      expect(database.connect).toHaveBeenCalledTimes(1);
      expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledTimes(1);
      expect(instance['_handleCheckDataBaseHealth']).toHaveBeenCalledWith({
        checkOperations: [{name: "strong", options: undefined}],
        client: clientMock,
        inputContext: undefined,
        healthCheckOptions: undefined
      });
      expect(clientMock.done).toHaveBeenCalledTimes(1);
    });
  }); // #checkDataBaseHealth

  describe('#_handleConnection', function () {
    it('Given connect to fail Then must return error', async function () {
      const instance = createInstance();

      const expectedError = new Error('Unit Test - Fake error');
      const dataBaseMock = {
        connect: jasmine.createSpy('connect').and.callFake(() => new Promise((resolve, reject) => setImmediate(reject, expectedError)))
      };
      instance['properties'].pgsqlOptions = {
        connectionUrl: 'pg://unit/test'
      };
      const pgsqlDriver = jasmine.createSpy('psqlDriver').and.returnValue(dataBaseMock);
      // @ts-ignore
      instance['properties'].pgsqlDriver = pgsqlDriver;

      try {
        await instance['_handleConnection']();
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(pgsqlDriver).toHaveBeenCalledTimes(1);
        expect(pgsqlDriver).toHaveBeenCalledWith('pg://unit/test');
        expect(dataBaseMock.connect).toHaveBeenCalledTimes(1);
      }
    });

    it('Given _checkDataBaseConnection to return invalid Then must return error', async function () {
      const instance = createInstance();

      const clientMock = {
          a: 65,
          done: jasmine.createSpy('done')
        },
        dataBaseMock = {
          connect: jasmine.createSpy('connect').and.callFake(() => new Promise(resolve => setImmediate(resolve, clientMock))),
          $pool: {
            end: jasmine.createSpy('end')
          }
        };
      instance['properties'].pgsqlOptions = {
        connectionUrl: 'pg://unit/test'
      };
      const pgsqlDriver = jasmine.createSpy('pgsqlDriver').and.returnValue(dataBaseMock);
      // @ts-ignore
      instance['properties'].pgsqlDriver = pgsqlDriver;

      // @ts-ignore
      spyOn(instance, '_checkDataBaseConnection').and.callFake(() => Promise.resolve({
        valid: false
      }));

      try {
        await instance['_handleConnection']();
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toEqual(jasmine.any(commonErrors.RequirementsNotSatisfiedError));
        expect(pgsqlDriver).toHaveBeenCalledTimes(1);
        expect(pgsqlDriver).toHaveBeenCalledWith('pg://unit/test');
        expect(dataBaseMock.connect).toHaveBeenCalledTimes(1);
        expect(instance['_checkDataBaseConnection']).toHaveBeenCalledWith({
          client: clientMock
        });
        expect(clientMock.done).toHaveBeenCalledTimes(1);
        expect(dataBaseMock.$pool.end).toHaveBeenCalledTimes(1);
      }
    });

    it('Given _isDataBaseInitialize to fail Then must return error', async function () {
      const instance = createInstance();

      const expectedError = new Error('Unit Test - Fake error');
      const clientMock = {
          a: 65,
          done: jasmine.createSpy('done')
        },
        dataBaseMock = {
          connect: jasmine.createSpy('connect').and.callFake(() => new Promise(resolve => setImmediate(resolve, clientMock))),
          $pool: {
            end: jasmine.createSpy('end')
          }
        };
      instance['properties'].pgsqlOptions = {
        connectionUrl: 'pg://unit/test'
      };
      const pgsqlDriver = jasmine.createSpy('pgsqlDriver').and.returnValue(dataBaseMock);
      // @ts-ignore
      instance['properties'].pgsqlDriver = pgsqlDriver;

      // @ts-ignore
      spyOn(instance, '_checkDataBaseConnection').and.callFake(() => Promise.resolve({
        valid: true
      }));

      // @ts-ignore
      spyOn(instance, '_isDataBaseInitialize').and.callFake(() => Promise.reject(expectedError));

      try {
        await instance['_handleConnection']();
        fail(new Error('Must not be called'));
      } catch (error) {
        expect(error).toBe(expectedError);
        expect(pgsqlDriver).toHaveBeenCalledTimes(1);
        expect(pgsqlDriver).toHaveBeenCalledWith('pg://unit/test');
        expect(dataBaseMock.connect).toHaveBeenCalledTimes(1);
        expect(instance['_checkDataBaseConnection']).toHaveBeenCalledWith({
          client: clientMock
        });
        expect(instance['_isDataBaseInitialize']).toHaveBeenCalledWith({
          client: clientMock
        });
        expect(clientMock.done).toHaveBeenCalledTimes(1);
        expect(dataBaseMock.$pool.end).toHaveBeenCalledTimes(1);
      }
    });

    it('Given _isDataBaseInitialize to return initialized=true Then must return success', async function () {
      const instance = createInstance();

      const clientMock = {
          a: 65,
          done: jasmine.createSpy('done')
        },
        dataBaseMock = {
          connect: jasmine.createSpy('connect').and.callFake(() => new Promise(resolve => setImmediate(resolve, clientMock))),
          $pool: {
            end: jasmine.createSpy('end')
          }
        };
      instance['properties'].pgsqlOptions = {
        connectionUrl: 'pg://unit/test'
      };
      const pgsqlDriver = jasmine.createSpy('pgsqlDriver').and.returnValue(dataBaseMock);
      // @ts-ignore
      instance['properties'].pgsqlDriver = pgsqlDriver;

      // @ts-ignore
      spyOn(instance, '_checkDataBaseConnection').and.callFake(() => Promise.resolve({
        valid: true
      }));

      // @ts-ignore
      spyOn(instance, '_isDataBaseInitialize').and.callFake(() => Promise.resolve({
        initialized: true
      }));

      // @ts-ignore
      spyOn(instance, '_handleDataBaseInitialization').and.callFake(() => Promise.reject(new Error('Must not bee called')));

      await instance['_handleConnection']();

      expect(pgsqlDriver).toHaveBeenCalledTimes(1);
      expect(pgsqlDriver).toHaveBeenCalledWith('pg://unit/test');
      expect(dataBaseMock.connect).toHaveBeenCalledTimes(1);
      expect(instance['_checkDataBaseConnection']).toHaveBeenCalledWith({
        client: clientMock
      });
      expect(instance['_isDataBaseInitialize']).toHaveBeenCalledWith({
        client: clientMock
      });
      expect(instance['_handleDataBaseInitialization']).not.toHaveBeenCalled();
      expect(clientMock.done).toHaveBeenCalledTimes(1);
      expect(dataBaseMock.$pool.end).not.toHaveBeenCalled();
      // @ts-ignore
      expect(instance['properties'].database).toBe(dataBaseMock);
    });

    it('Given _isDataBaseInitialize to return initialized=false Then must return success and call _handleDataBaseInitialization', async function () {
      const instance = createInstance();

      const clientMock = {
          a: 65,
          done: jasmine.createSpy('done')
        },
        dataBaseMock = {
          connect: jasmine.createSpy('connect').and.callFake(() => new Promise(resolve => setImmediate(resolve, clientMock))),
          $pool: {
            end: jasmine.createSpy('end')
          }
        };
      instance['properties'].pgsqlOptions = {
        connectionUrl: 'pg://unit/test'
      };
      const pgsqlDriver = jasmine.createSpy('pgsqlDriver').and.returnValue(dataBaseMock);
      // @ts-ignore
      instance['properties'].pgsqlDriver = pgsqlDriver;

      // @ts-ignore
      spyOn(instance, '_checkDataBaseConnection').and.callFake(() => Promise.resolve({
        valid: true
      }));

      // @ts-ignore
      spyOn(instance, '_isDataBaseInitialize').and.callFake(() => Promise.resolve({
        initialized: false
      }));

      // @ts-ignore
      spyOn(instance, '_handleDataBaseInitialization').and.callFake(() => Promise.resolve());

      await instance['_handleConnection']();
      expect(pgsqlDriver).toHaveBeenCalledTimes(1);
      expect(pgsqlDriver).toHaveBeenCalledWith('pg://unit/test');
      expect(dataBaseMock.connect).toHaveBeenCalledTimes(1);
      expect(instance['_checkDataBaseConnection']).toHaveBeenCalledWith({
        client: clientMock
      });
      expect(instance['_isDataBaseInitialize']).toHaveBeenCalledWith({
        client: clientMock
      });
      expect(instance['_handleDataBaseInitialization']).toHaveBeenCalledWith({
        client: clientMock
      });
      expect(clientMock.done).toHaveBeenCalledTimes(1);
      expect(dataBaseMock.$pool.end).not.toHaveBeenCalled();
      // @ts-ignore
      expect(instance['properties'].database).toBe(dataBaseMock);
    });
  }); // #_handleConnection
});
