import {SimpleTestLauncher, configs} from 'zrim-test-bootstrap';
import * as pathUtils from 'path';

const configBuilder = new configs.SimpleTestLauncherConfigBuilder();
const rootDirectoryPath = pathUtils.resolve(__dirname + '/../../..');

configBuilder
  .projectConfiguration()
  .rootDirectoryPath(rootDirectoryPath)
  .parentBuilder()
  .testConfiguration()
  .integrationTest()
  .specDirPath(pathUtils.relative(rootDirectoryPath, __dirname))
  .parentBuilder();

SimpleTestLauncher.execute(configBuilder.build());
