import {SimplePostGreSqlManager} from '../../../lib/simple-pgsql-manager';

describe('#SimplePostGreSqlManager', function () {

  const _context = {
    instance: new SimplePostGreSqlManager(),
    lastTestSucceed: true
  };

  function asyncTest(description: string, fn: Function, failedAfterMs?: number) {
    it(description, async function () {
      if (!_context.lastTestSucceed) {
        fail(new Error("Last test failed"));
      } else {
        _context.lastTestSucceed = false;
        await fn.call(this);
      }
    }, failedAfterMs || 5000);
  }

  describe('#instance', function () {
    asyncTest("#initialize", async function () {
      const connectionUrl = process.env.ZE_PGSQL_CONN_URL || 'postgresql://zadmin:qwerty@127.0.0.1:5432/zadmin';

      await _context.instance.initialize({
        connectionUrl
      });
      expect(true).toBeTruthy();
      _context.lastTestSucceed = true;
    }); // #initialize

    asyncTest("#connect", async function () {
      await _context.instance.connect();
      expect(true).toBeTruthy();
      _context.lastTestSucceed = true;
    }); // #connect
  }); // #instance

  describe('#workflow', function () {
    asyncTest("#select-1", async function () {
      const client = await _context.instance.database.connect();

      const response = await client.query('SELECT datname FROM pg_database WHERE datistemplate = false');

      expect(response).toEqual(jasmine.arrayContaining([{
        datname: 'postgres'
      }, {
        datname: 'zadmin'
      }]));

      _context.lastTestSucceed = true;
    }); // #connect
  }); // #workflow

  describe('#cleanUp', function () {
    it(`#disconnect`, async function () {
      if (_context.instance.isConnected()) {
        await _context.instance.disconnect();
      }
      expect(true).toBeTruthy();
    });
  }); // #cleanUp
});
