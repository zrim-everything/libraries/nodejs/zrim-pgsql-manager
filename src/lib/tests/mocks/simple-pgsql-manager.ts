import {SimpleConnectableObject} from "zrim-base-objects";
import {postGreSqlManager, PostGreSqlManager} from "../../pgsql-manager";
import {common as commonErrors} from 'zrim-errors';
import * as pgPromise from 'pg-promise';

/**
 * Simple implementation of the postgresql manager (MOCK)
 */
export class SimplePostGreSqlManager extends SimpleConnectableObject implements PostGreSqlManager {

  /**
   * @inheritDoc
   */
  public flags: postGreSqlManager.Flags | undefined = undefined;

  /**
   * @inheritDoc
   */
  public database: pgPromise.IDatabase<postGreSqlManager.DriverExtension> | undefined = undefined;

  /**
   * @inheritDoc
   */
  public getFlags(): postGreSqlManager.Flags | undefined {
    return {};
  }

  /**
   * @inheritDoc
   */
  public isReadOnly(): boolean {
    return false;
  }

  /**
   * @inheritDoc
   */
  public async checkDataBaseHealth(options: postGreSqlManager.CheckDataBaseHealthOptions): Promise<postGreSqlManager.CheckDataBaseHealthOnResolve> {
    throw new commonErrors.NotImplementedError();
  }
}
