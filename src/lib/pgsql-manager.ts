import * as pgPromise from 'pg-promise';

export namespace postGreSqlManager {

  export interface DriverExtension {

  }

  export interface EmptyDriverExtension extends DriverExtension {

  }

  export interface Flags {
    /**
     * Is the database readonly
     */
    readOnly?: boolean | undefined | null;
  }

  export interface CheckOperations {
    /**
     * The operation name
     */
    name: string;
    /**
     * Possible options to give for this operation
     */
    options?: object;
  }

  export interface CheckDataBaseHealthOptions {
    /**
     * An array that checks if the operation is strong/weak
     */
    checkOperations: CheckOperations[];
    /**
     * The context of the service that called this function
     */
    inputContext?: any;
    /**
     * Possible input for additional options if the methods needs to be overwrite.
     */
    healthCheckOptions?: object;
  }

  export interface CheckDataBaseHealthOnResolve {
    /**
     * flag for database healthy
     */
    healthy: boolean;

    /**
     * The detail result for database check
     */
    checkHealthResult?: object | undefined;
  }
}

export interface PostGreSqlManager {

  /**
   * The flags
   */
  readonly flags: postGreSqlManager.Flags | undefined;

  /**
   * The database
   */
  readonly database?: pgPromise.IDatabase<postGreSqlManager.DriverExtension> | undefined;

  /**
   * Returns the flags
   */
  getFlags(): postGreSqlManager.Flags | undefined;

  /**
   * Test if the manager is readonly
   */
  isReadOnly(): boolean;

  /**
   * Check the database health
   * @param options
   */
  checkDataBaseHealth(options: postGreSqlManager.CheckDataBaseHealthOptions): Promise<postGreSqlManager.CheckDataBaseHealthOnResolve>;
}
