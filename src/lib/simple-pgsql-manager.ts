import {connectableObject, initializableObject, SimpleConnectableObject} from "zrim-base-objects";
import {postGreSqlManager, PostGreSqlManager} from "./pgsql-manager";
import {common as commonErrors} from 'zrim-errors';
import * as pgPromise from 'pg-promise';
import * as _ from 'lodash';
import {ProxyLogger} from 'zrim-proxy-logger';

export namespace simplePostGreSqlManager {

  export interface Properties extends connectableObject.Properties {
    /**
     * The postgresql options
     */
    pgsqlOptions: PostGreSqlOptions;
    /**
     * The driver to use
     */
    pgsqlDriver?: pgPromise.IMain;

    /**
     * Flags
     */
    flags?: postGreSqlManager.Flags;

    /**
     * The database
     */
    database?: pgPromise.IDatabase<postGreSqlManager.DriverExtension>;
  }

  export interface PostGreSqlOptions {
    /**
     * The connection string (url mode)
     */
    connectionUrl: string;
  }

  export interface InitializeOptions extends initializableObject.InitializeOptions {
    /**
     *  The native options for the pg-promise driver
     */
    nativeDriverOptions?: object | undefined | null;
    /**
     * The connection string (url mode)
     */
    connectionUrl: string;
    /**
     * flags for the instance
     */
    flags?: postGreSqlManager.Flags | undefined | null;
  }

  export interface CheckDataBaseConnectionOptions {
    /**
     * The client
     */
    client: pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;
  }

  export interface CheckDataBaseConnectionOnResolve {
    /**
     * flag for valid database
     */
    valid: boolean;
  }

  export interface IsDataBaseInitializedOptions {
    /**
     * The client
     */
    client: pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;
  }

  export interface IsDataBaseInitializedOnResolve {
    /**
     * true if the database is initialized
     */
    initialized: boolean;
  }

  export interface HandleDataBaseInitializationOptions {
    /**
     * The client
     */
    client: pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension>;
  }

  export interface HandleDataBaseInitializationOnResolve {

  }

  export interface HandleCheckDataBaseHealthOptions {
    /**
     * Possible input the connection client to use to make the query. Null if we could not connect to the database
     */
    client?: pgPromise.IBaseProtocol<postGreSqlManager.DriverExtension> | undefined | null;
    /**
     * Possible input the error of the connection. Null if there were no problem
     */
    connectionError?: Error | undefined | null;
    /**
     * An array that checks if the operation is strong/weak
     */
    checkOperations: postGreSqlManager.CheckOperations[];
    /**
     * Possible input for additional options if the methods needs to be overwrite.
     */
    healthCheckOptions?: any | undefined | null;

    /**
     * The input context
     */
    inputContext?: any;
  }

  export interface HandleCheckDataBaseHealthOnResolve {
    /**
     * flag for database healthy
     */
    healthy: boolean;

    /**
     * The check result
     */
    checkHealthResult?: any;
    /**
     * The possible error explication
     */
    error?: Error | undefined | null;
  }
}

/**
 * Simple implementation of the postgresql manager
 */
export class SimplePostGreSqlManager extends SimpleConnectableObject implements PostGreSqlManager {

  /**
   * @inheritDoc
   */
  protected properties: simplePostGreSqlManager.Properties;

  /**
   * @inheritDoc
   */
  public get flags(): postGreSqlManager.Flags | undefined {
    return this.getFlags();
  }

  /**
   * @inheritDoc
   */
  public get database(): pgPromise.IDatabase<postGreSqlManager.DriverExtension> | undefined {
    return this.properties.database;
  }

  /**
   * @inheritDoc
   */
  public getFlags(): postGreSqlManager.Flags | undefined {
    return _.merge({}, this.properties.flags);
  }

  /**
   * @inheritDoc
   */
  public isReadOnly(): boolean {
    const flags = this.getFlags();
    const readOnly = _.get(flags, 'readOnly', false) === true;
    return readOnly;
  }

  /**
   * @inheritDoc
   */
  public async initialize(options: simplePostGreSqlManager.InitializeOptions): Promise<void> {
    return await super.initialize(options);
  }

  /**
   * @inheritDoc
   */
  public async checkDataBaseHealth(options: postGreSqlManager.CheckDataBaseHealthOptions): Promise<postGreSqlManager.CheckDataBaseHealthOnResolve> {
    const logger = (_.get(options, 'inputContext.logger') || this.logger).of({prefixes: ['pushMessages']}) as ProxyLogger;

    if (!this.isReady()) {
      const activeStateNames = this.getActivatedStateNames().join(',');
      logger.debug(`Manager not ready for a health check. Active state names are '${activeStateNames}'`);
      throw new commonErrors.NotReadyError(`Not ready. Active states '${activeStateNames}'`);
    }

    const checkOptions = {
      checkOperations: options.checkOperations,
      inputContext: options.inputContext,
      healthCheckOptions: options.healthCheckOptions
    } as simplePostGreSqlManager.HandleCheckDataBaseHealthOptions;

    let client: pgPromise.IConnected<postGreSqlManager.DriverExtension>;
    try {
      client = await this.properties.database.connect();
      logger.debug("Get connection success");
      checkOptions.client = client;
    } catch (error) {
      logger.debug({error}, `Failed to connection : ${error.message}`);
      checkOptions.connectionError = error;
    }

    try {
      const {healthy, checkHealthResult} = await this._handleCheckDataBaseHealth(checkOptions);
      logger.debug("Release the connection");
      if (client) {
        client.done();
      }
      client = undefined;
      return {
        healthy,
        checkHealthResult
      };
    } catch (error) {
      logger.debug({error}, `Fails during the health check: ${error.message}`);
      if (client) {
        logger.debug("Release the connection");
        client.done();
      }
      throw error;
    }
  }

  /**
   * @inheritDoc
   */
  protected async _handleInitialization(options: simplePostGreSqlManager.InitializeOptions, ...args: any[]): Promise<void> {
    await super._handleInitialization(options, ...args);

    if (!/^postgresql:\/\//.test(options.connectionUrl)) {
      throw new commonErrors.IllegalArgumentError(`Invalid connection url '${options.connectionUrl}'`);
    }

    const pgsqlDriver = require('pg-promise')(options.nativeDriverOptions || {});

    // Now copy the options
    this.properties.pgsqlDriver = pgsqlDriver;
    this.properties.pgsqlOptions = {
      connectionUrl: options.connectionUrl
    };
    this.properties.flags = _.merge({}, options.flags);
  }

  /**
   * @inheritDoc
   */
  protected async _handleDisconnection(...args: any[]): Promise<void> {
    const logger = this.logger;
    logger.debug({prefixes: ['_handleDisconnection']}, "Close the database connection pool");
    this.properties.database.$pool.end();
    delete this.properties.database;
  }

  /**
   * Check if the connection is valid
   * @param options
   */
  protected async _checkDataBaseConnection(options: simplePostGreSqlManager.CheckDataBaseConnectionOptions):
    Promise<simplePostGreSqlManager.CheckDataBaseConnectionOnResolve> {
    const logger = this.logger.of({prefixes: ['_checkDataBaseConnection']});

    try {
      logger.debug("Do a simple SELECT 1");
      await options.client.query('SELECT 1');
      logger.debug("Execution succeed");
      return {
        valid: true
      };
    } catch (error) {
      logger.warn({error}, `Failed to query the database: ${error.message}`);
      return {
        valid: false
      };
    }
  }

  /**
   * Check if teh database is initialized
   * @param options
   */
  protected async _isDataBaseInitialize(options: simplePostGreSqlManager.IsDataBaseInitializedOptions):
    Promise<simplePostGreSqlManager.IsDataBaseInitializedOnResolve> {
    const logger = this.logger.of({prefixes: ['_isDataBaseInitialize']});
    logger.debug("Not configured.");
    return {
      initialized: true
    };
  }

  protected async _handleDataBaseInitialization(options: simplePostGreSqlManager.HandleDataBaseInitializationOptions):
    Promise<simplePostGreSqlManager.HandleDataBaseInitializationOnResolve> {
    const logger = this.logger.of({prefixes: ['_handleDataBaseInitialization']});
    logger.debug("Nothing to initialize");
    return {};
  }

  /**
   * @inheritDoc
   */
  protected async _handleConnection(...args: any[]): Promise<void> {
    const logger = this.logger.of({prefixes: ['_handleConnection']});

    logger.debug(`Create the database`);
    const database = this.properties.pgsqlDriver(this.properties.pgsqlOptions.connectionUrl) as pgPromise.IDatabase<postGreSqlManager.DriverExtension>;

    let client: pgPromise.IConnected<postGreSqlManager.DriverExtension>;
    try {
      logger.debug(`Call the connect`);
      client = await database.connect();
    } catch (error) {
      logger.debug({error}, `Failed to connection : ${error.message}`);
      throw error;
    }

    try {
      logger.debug( `Call the check handler`);
      const checkDbResponse = await this._checkDataBaseConnection({
        client
      });

      if (checkDbResponse.valid !== true) {
        logger.debug("Connection invalid");
        throw new commonErrors.RequirementsNotSatisfiedError('The connection established is invalid');
      }

      logger.debug("Connection valid");
      const {initialized} = await this._isDataBaseInitialize({
        client
      });

      if (initialized !== true) {
        logger.debug("The database need to be initialized");
        await this._handleDataBaseInitialization({
          client
        });
        logger.debug(`Database initialization done with success`);
      }

      logger.debug("Connection OK and DataBase initialized");
      logger.debug("Release the connection");
      client.done();
      this.properties.database = database;
    } catch (error) {
      if (!(error instanceof commonErrors.RequirementsNotSatisfiedError)) {
        logger.debug({error}, `Check failed: ${error.message}`);
      }
      logger.debug("Release the connection");
      client.done();

      logger.debug("End the database");
      database.$pool.end();
      throw error;
    }
  }

  /**
   * Handle the database health check
   * @param options
   */
  protected async _handleCheckDataBaseHealth(options: simplePostGreSqlManager.HandleCheckDataBaseHealthOptions):
    Promise<simplePostGreSqlManager.HandleCheckDataBaseHealthOnResolve> {
    const logger = this.logger.of({prefixes: ['_handleCheckDataBaseHealth']});

    if (!options.client) {
      logger.debug(`No client connection provided`);
      return {
        healthy: false,
        error: options.connectionError
      };
    }

    try {
      logger.debug("Do a simple SELECT 1");
      await options.client.query('SELECT 1');
      logger.debug("Execution succeed");
      return {
        healthy: true
      };
    } catch (error) {
      logger.debug({error}, `Failed to query the database: ${error.message}`);
      return {
        healthy: false,
        error
      };
    }
  }
}
