#!/usr/bin/env bash

#
# Launch unit test
#

set -x

npm-install --all
install_exit_code=$?
if [[ $install_exit_code -ne 0 ]]; then
  echo "Npm install failed: exit code '$install_exit_code'"
  exit 1
fi

npm run unitTest
test_exit_code=$?

if [[ $test_exit_code -ne 0 ]]; then
  echo "Test failed: exit code '$test_exit_code'"
  exit 1
fi
